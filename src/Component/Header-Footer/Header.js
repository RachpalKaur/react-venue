import React, { useState, useEffect } from "react";
import Sidedrawer from "./Sidedrawer"


import { AppBar, Toolbar, IconButton } from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";



const Header = () => {

const [ drawerOpen, satDrawerOpen ] = useState(false);
const [ headerShow, setHeaderSHow ] = useState(false);

const handleScroll = () =>  {
    if(window.scrollY) {
        setHeaderSHow(true)
    }else{
        setHeaderSHow(false)
    }
}

const toggleDrawer = (value) => {
    satDrawerOpen(value)
}

useEffect((value) =>{
window.addEventListener('scroll', handleScroll)
},[])

    return(
        <>
        <AppBar
        position="fixed"
        style={{
            backgroundColor:headerShow ? '#2f2f2f': 'transparent',
            boxShadow:'none',
            padding:'10px 0px',
        }}
        >
            <Toolbar>
                <div className="header_logo">
                    <div className="font_righteous header_logo_venue">The Venue</div>
                    <div className="header_logo_title">Musical Event</div>
                </div>
                <IconButton
                aria-label="Menu"
                onClick={()=> toggleDrawer(true)}
                style={{
                    color:"#ffffff"
                }}
                >
                 <MenuIcon />       
                </IconButton>
                <Sidedrawer 
                open={drawerOpen}
                onClose={(value)=> toggleDrawer(value)}
                />
            </Toolbar>

        </AppBar>
         </>
    )
}

export default Header;