import React from 'react';
import Slider from "react-slick";

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import slide_one from "../../resources/images/slide_one.jpg";
import slide_two from "../../resources/images/slide_two.jpg";
import slide_three from "../../resources/images/slide_three.jpg";

const Carrousel = () =>{

const Setting = {
    dots:false,
    infinite:true,
    autoplay:true,
    speed:500,

}

    return(
        <div className="carrousel-wrapper"
        style={{
            height:'${window.innerHeight}px'
        }}
        >
            <Slider {...Setting}>
                <div>
                <div 
                className="carrousel_image"
                style={{
                    backgroundImage: `url(${slide_one})`,
                    height:'700px',
                   }}
                >
                </div>
                </div>
                <div>
                <div 
                className="carrousel_image"
                style={{
                    backgroundImage: `url(${slide_two})`,
                    height:'700px',
                   }}
                >
                </div>
                </div>
                <div>
                <div 
                className="carrousel_image"
                style={{
                    backgroundImage: `url(${slide_three})`,
                    height:'700px',
                   }}
                >
                </div>
                </div>
                
                
            </Slider>

        </div>
    )
}

export default Carrousel;