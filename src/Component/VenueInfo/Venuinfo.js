import React from 'react';
import { Zoom } from 'react-awesome-reveal';

import icon_calender from '../../resources/images/Icons/calendar.png';
import icon_location from '../../resources/images/Icons/location.png';

const Venuinfo = () => {
    return(
        <div className="bck_black">
           <div className="center_wrapper">
               <div className="vn_wrapper">
                <Zoom className="vn_item">
                    <>
                    <div className="vn_outer">
                        <div className="vn_inner">
                            <div className="vn_icon_square bck_red"></div>
                            <div 
                            className="vn_icon"
                            style={{
                                backgroundImage:`url(${icon_calender})`
                            }}
                            ></div>
                            <div className="vn_title">Event Date & Time</div>
                            <div className="vn_desc">Dec 20 @01:20:00pm</div>
                        </div>
                    </div>
                    </>
                </Zoom>
                <Zoom className="vn_item" delay={500}>
                    <>
                    <div className="vn_outer">
                        <div className="vn_inner">
                            <div className="vn_icon_square bck_yellow"></div>
                            <div 
                            className="vn_icon"
                            style={{
                                backgroundImage:`url(${icon_location})`
                            }}
                            ></div>
                            <div className="vn_title">Event Location</div>
                            <div className="vn_desc">375 Deer Oak, Okland, CA9878</div>
                        </div>
                    </div>
                    </>
                </Zoom>
               </div>
           </div>
        </div>
    )
}
export default Venuinfo;