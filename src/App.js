import React from "react";

import Header from "./Component/Header-Footer/Header";
import Footer from "./Component/Header-Footer/Footer";
import Featured from "./Component/Featured";
import "./resources/style.css";
import Venuinfo from "./Component/VenueInfo/Venuinfo";
import Mainindex from "./Component/Highlights/Mainindex";



function App() {
  return (
    <>
    <Header />
    <>
    <Featured />
    </>
    <>
    <Venuinfo />
    </>
    <>
    <Mainindex/>
    </>

    <Footer />
    </>
  );
}

export default App;
